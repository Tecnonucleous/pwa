# pwa

Progressive Web App de Tecnonucleous.com

### Configuracion

Disponemos de dos métodos para configurar el Service Worker:

#### Recompilando por haxe

Este es el método más limpio, necesitareis haxe, haxelib e instalar la librería compiletime: ```haxelib install compiletime```.
Luego configuráis el apartado *sw* y recompilais el Service Worker con ```haxe build.hxml```.

#### Modificando sw.js

En el apartado var ```Main = function() {```, asignáis la variable manifest a nula(*var manifest = null*) y modificáis las otras variables con vuestra configuracion.

#### Configuración

+ **default_cache:** Paginas a cachear desde un primer momento.
+ **blacklist:** Paginas que nunca se deben cachear. *EJ: /admin, /api, paginas dinámicas...*
+ **max_elements:** Máxima paginas a cachear(Los navegadores disponen de poco espacio en cache).
+ **cache_articles:** Cuantos artículos nuevos a cachear desde un primer momento.

+ **vapid_key:** Llave vapid publica de push, en formato array de Int.
+ **register_endpoint:** Pagina rest que se llamara para entregar los datos del registro push y estadísticas.

+ **version:** Versión de la PWA, *Recomendable cambiarla cada vez que se quiera reiniciar el cache*.
+ **min_version:** Primera versión puesta en producción de la PWA.

+ **api_endpoint:** URL de la API de ghost.
+ **articles_url:** URL donde se encuentran los artículos.
+ **client_id:** Identificador del cliente de la API de ghost (*Se extrae del código fuente de la página principal*).
+ **client_secret:** Clave del cliente de la API de ghost (*Se extrae del código fuente de la página principal*).

### Intalación

Copiamos los archivos de la carpeta JS (sw.js y sw.js.map) en el directorio root de nuestro tema.
Además del archivo "tecnonucleous.webmanifest" que tendreis que modificar para vuetra web añadiendo vuestros datos.

> .../ghost/content/theme/<carpeta del tema>

Después creamos la carpeta "src" en esa misma ruta y pagamos los archivos "Main.hex" y "build.hxml"

Entramos en Ghost "Code injection" y en "Blog Header" pegamos el siguiente código:

```
<!-- PWA -->
<link rel="manifest" href="https://tecnonucleous.com/tecnonucleous.webmanifest" />
<script>
if('serviceWorker' in navigator) {
    navigator.serviceWorker.register('https://tecnonucleous.com/sw.js',  {scope: 'https://tecnonucleous.com/'});
    };
</script>
```

⚠️ Importante cambiar `https://tecnonucleous.com` por la url de vuestra web