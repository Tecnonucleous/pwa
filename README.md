# pwa

Progressive Web App for Tecnonucleous.com

### Configuration

TO DO, see Spanish version.

### Installation

Copy the files in the JS folder (sw.js and sw.js.map) in the root directory of our theme.
In addition to the file "tecnonucleous.webmanifest" that you will have to modify for the web by adding your data.

> .../ghost/content/theme/<theme folder>

Before create the "src" folder in that same path and paste the files "Main.hex" and "build.hxml"

Finally, enter in your Ghost website in "Code injection" and in "Blog Header" we paste the following code:

```
<!-- PWA -->
<link rel="manifest" href="https://tecnonucleous.com/tecnonucleous.webmanifest" />
<script>
if('serviceWorker' in navigator) {
    navigator.serviceWorker.register('https://tecnonucleous.com/sw.js',  {scope: 'https://tecnonucleous.com/'});
    };
</script>
```

⚠️ Important change `https://tecnonucleous.com` by the URL of your website