
class Main {
    //Cache
    #if test
    var default_cache = ['test/cat-1.jpg', 'test/cat-2.jpg', 'test/cat-3.jpg'];
    var blacklist = ['test/notacat.jpg'];
    #else
    var default_cache = ['/offline/'];
    var blacklist = [];
    #end
    var max_elements = 40;
    var cache_articles = 10;

    //Push
    var vapid_key = new js.html.Uint8Array([]);
    var register_endpoint = '';

    //SW
    var version = 1;
    var min_version = 0;

    //GHOST
    var api_endpoint = '/ghost/api/v0.1/';
    var articles_url = '/';
    var client_id = 'ghost-frontend';
    var client_secret = '';


    var worker: js.html.ServiceWorkerGlobalScope = js.Lib.global;
    var final_cache: Array<String> = [];

    public static function main() {
        new Main();
    }

    public function new() {
        var manifest = CompileTime.parseJsonFile('../tecnonucleous.webmanifest');
        if(manifest != null && manifest.sw != null) {
            if(manifest.sw.default_cache != null) this.default_cache = manifest.sw.default_cache;
            if(manifest.sw.blacklist != null) this.blacklist = manifest.sw.blacklist;
            if(manifest.sw.max_elements != null) this.max_elements = manifest.sw.max_elements;
            if(manifest.sw.cache_articles != null) this.cache_articles = manifest.sw.cache_articles;

            if(manifest.sw.vapid_key != null) this.vapid_key = new js.html.Uint8Array(manifest.sw.vapid_key);
            if(manifest.sw.register_endpoint != null) this.register_endpoint = manifest.sw.register_endpoint;

            if(manifest.sw.min_version != null) this.min_version = manifest.sw.min_version;
            if(manifest.sw.version != null) this.version = manifest.sw.version;

            if(manifest.sw.api_endpoint != null) this.api_endpoint = manifest.sw.api_endpoint;
            if(manifest.sw.articles_url != null) this.articles_url = manifest.sw.articles_url;
            if(manifest.sw.client_id != null) this.client_id = manifest.sw.client_id;
            if(manifest.sw.client_secret != null) this.client_secret = manifest.sw.client_secret;

        }

        this.final_cache = this.default_cache;

        register_cache();
        //register_push();
    }

    public function add_basic_cache(cache:Dynamic): js.Promise<Void> {
        return cache.addAll(this.final_cache);
    }

    public inline function articles_cache(): js.Promise<Array<String>> {
        return this.worker.fetch(this.api_endpoint + 'posts/?client_id=' + this.client_id + '&client_secret=' + this.client_secret + '&limit=' + this.cache_articles).then(function(response: js.html.Response): js.Promise<Array<String>> {
            return response.json().then(function(data): Array<String> {
                var posts : Array<{url: String}> = data.posts;
                for(post in posts) {
                    this.final_cache.push(post.url);
                }

                return this.final_cache;
            });
        }).catchError(function(err): Array<String> {
            trace('Error al cachear articulos: ' + err);
            return [];
        });
    }

    public inline function open_cache<T>(f: Dynamic -> js.Promise<T>): js.Promise<T> {
        return untyped caches.open('tecnonucleous-' + this.version).then(f);
    }

    public inline function get_content(request: js.html.Request): js.Promise<js.html.Response> {
        return open_cache(function(cache: Dynamic): js.Promise<js.html.Response> { 
            return cache.match(request).then(function (matching) {
                if(matching != null) {
                    return matching;
                } else if(matching == null && this.worker.navigator.onLine) {
                    return this.worker.fetch(request);
                } else {
                    return this.get_offline(cache);
                    //return this.worker.fetch(request).catchError(this.get_offline);
                }
            });
        }); 
    }

    public inline function get_offline(cache: Dynamic, ?e:Dynamic): js.Promise<js.html.Response> {
        #if test
            return cache.match('test/cat-2.jpg');
        #else
            return cache.match('/offline/');
        #end
    }

    public inline function update_cache(id: String, request: js.html.Request, old_response: js.html.Response): js.Promise<Dynamic> {
        return open_cache(function (cache) {
            return this.worker.fetch(request).then(function (response: js.html.Response) {
                return cache.put(request, response.clone()).then(function(d) {
                    if(old_response != null) {
                        if(response.headers.get('ETag') != old_response.headers.get('ETag')) {
                            trace('Refrescando pagina...');
                            this.worker.clients.get(id).then(function(client: js.html.Client) {
                                client.postMessage('refresh');
                            });
                        }
                    }
                    return d;
                });
            });
        });
    }

    public inline function remove_old_cache(): js.Promise<Dynamic> {
        var promises: Array<js.Promise<Dynamic>> = [];
        for(v in this.min_version ... this.version) {
            trace('borrando cache ' + v);
            promises.push(untyped caches.delete('tecnonucleous-' + v));
        }

        promises.push(open_cache(function(cache:Dynamic): js.Promise<Dynamic> {
            return cache.keys().then(function(keys: Array<js.html.Request>) {
                var promises2: Array<js.Promise<Bool>> = [];
                for(k in keys) {
                    var dont_remove = false;
                    for(m in this.final_cache) {
                        if(k.url.indexOf(m) != -1) {
                            dont_remove = true;
                        }
                    }

                    if(!dont_remove) promises2.push(cache.delete(k));
                }
                return js.Promise.all(promises2);
            });
        }));

        return js.Promise.all(promises);
    }

    public inline function check_blacklist(url: String): Bool {
        var check = true;
        for(s in this.blacklist) {
            if(url.indexOf(s) == -1) check = false;
        }

        return check;
    }

    public inline function check_max_cache(evt: js.html.FetchEvent): js.Promise<Dynamic> {
        return open_cache(function(cache) {
            return cache.keys().then(function(k: Array<js.html.Request>) {
                if(k.length > this.max_elements) evt.waitUntil(this.remove_old_cache());
                return k;
            });
        });
    }

    public function register_cache() {
        this.worker.addEventListener('install', function(evt) {
            trace('SW de Tecnonucleous instalado.');

            evt.waitUntil(this.articles_cache().then(function(cache): js.Promise<Dynamic> {
                return open_cache(this.add_basic_cache);
            }));
            evt.waitUntil(cast(remove_old_cache()));
        });

        this.worker.addEventListener('fetch', function(evt:js.html.FetchEvent) {
            if(evt.request.method == 'GET' && this.check_blacklist(evt.request.url)) {
                trace('Sirviendo datos...');
                var content = get_content(evt.request);

                evt.respondWith(content);
                evt.waitUntil(content.then(function(c) {
                    return update_cache(evt.clientId, evt.request, c);
                }));
                evt.waitUntil(check_max_cache(evt));
            } else {
                trace('La peticion no se puede cachear.');
            }
        });

        this.worker.addEventListener('activate', function(evt) {
            trace('Activando SW de Tecnonucleous.');

            evt.waitUntil(this.remove_old_cache());
        });
    }

    public inline function register_sub(subscription: Dynamic)  {
        var body_o = {
            sub: subscription,
            stats: {
                langs: this.worker.navigator.languages,
                platforms: this.worker.navigator.platform,
                product: this.worker.navigator.product,
                useragent: this.worker.navigator.userAgent,
                perfomance: this.worker.performance.timing
            } 
        };

        this.worker.fetch(this.register_endpoint, {
            method: 'post',
            headers: [['Content-type', 'application/json']],
            body: haxe.Json.stringify(body_o)
        });
    }

    public function register_push() {
        untyped this.worker.registration.pushManager.getSubscription().then(function(sub) {
            if(sub != null) return;

            untyped this.worker.registration.pushManager.subscribe({
                userVisibleOnly: true,
                applicationServerKey: vapid_key
            }).then(this.register_sub);
        });

        this.worker.addEventListener('push', function(evt) {
            var data: {title: String, icon: String, img: String, body: String} = untyped evt.data.json();
            untyped this.worker.registration.showNotification(data.title, {body: data.body, icon: data.icon, image: data.img});
        });
    }
}